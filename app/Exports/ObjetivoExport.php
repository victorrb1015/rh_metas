<?php

namespace App\Exports;

use App\Objetivos;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class ObjetivoExport implements FromArray, WithHeadings, WithColumnFormatting, ShouldAutoSize
{
    protected $Objetivos;

    public function headings(): array
    {
        return [
            '#',
            'Objetivo',
            'Descripción',
            'Kpi meta',
            'Avance',
            'Estrategias'
        ];
    }

    public function __construct(array $Objetivos)
    {
        $this->Objetivos = $Objetivos;
    }

    public function array(): array
    {
        return $this->Objetivos;
    }
    public function columnFormats(): array
    {
        return [
            'B' => NumberFormat::FORMAT_GENERAL ,
            'C' => NumberFormat::FORMAT_GENERAL ,
            'D' => NumberFormat::FORMAT_GENERAL ,
            'E' => NumberFormat::FORMAT_PERCENTAGE,
        ];
    }
}
