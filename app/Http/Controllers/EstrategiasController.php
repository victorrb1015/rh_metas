<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateEstrategiasRequest;
use App\Http\Requests\UpdateEstrategiasRequest;
use App\Models\Estrategias;
use App\Repositories\EstrategiasRepository;
use DateTime;
use Flash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Response;

class EstrategiasController extends AppBaseController
{
    /** @var  EstrategiasRepository */
    private $estrategiasRepository;

    public function __construct(EstrategiasRepository $estrategiasRepo)
    {
        $this->estrategiasRepository = $estrategiasRepo;
    }

    public function index(Request $request)
    {
        //$estrategias = $this->estrategiasRepository->all();
            $objetivos = $this->estrategiasRepository->objetivos();
        $estrategias = DB::table('estrategia')
            ->join('objetivos', 'estrategia.id_obj', '=', 'objetivos.id')
            ->select('estrategia.*', 'objetivos.objetivo')
            ->Where('estrategia.deleted_at','=',NULL)
            ->get();


        return view('estrategias.index')
            ->with('estrategia', $estrategias)
            ->with('objetivos', $objetivos);
    }

    /**
     * Show the form for creating a new Estrategias.
     *
     * @return Response
     */
    public function create()
    {
        return view('estrategias.create');
    }

    /**
     * Store a newly created Estrategias in storage.
     *
     * @param CreateEstrategiasRequest $request
     *
     * @return Response
     */
    public function store(CreateEstrategiasRequest $request)
    {
        $input = $request->all();

        $estrategias = $this->estrategiasRepository->create($input);

        Flash::success('Estrategia almacenada correctamente.');

        return redirect(route('estrategias.index'));
    }

    /**
     * Display the specified Estrategias.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $estrategias = $this->estrategiasRepository->find($id);

        if (empty($estrategias)) {
            Flash::error('Estrategias not found');

            return redirect(route('estrategias.index'));
        }

        return view('estrategias.show')->with('estrategias', $estrategias);
    }

    /**
     * Show the form for editing the specified Estrategias.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $estrategias = $this->estrategiasRepository->find($id);
        $objetivos = $this->estrategiasRepository->objetivos();

        if (empty($estrategias)) {
            Flash::error('Estrategia no encontrada');

            return redirect(route('estrategias.index'));
        }

        return view('estrategias.edit')->with('estrategias', $estrategias)->with('objetivos',$objetivos);
    }

    /**
     * Update the specified Estrategias in storage.
     *
     * @param int $id
     * @param UpdateEstrategiasRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateEstrategiasRequest $request)
    {
        $estrategias = $this->estrategiasRepository->find($id);

        if (empty($estrategias)) {
            Flash::error('Estrategia no encontrada');

            return redirect(route('estrategias.index'));
        }

        $estrategias = $this->estrategiasRepository->update($request->all(), $id);

        Flash::success('Estrategia actualizada correctamente.');

        return redirect(route('estrategias.index'));
    }

    /**
     * Remove the specified Estrategias from storage.
     *
     * @param int $id
     *
     * @return Response
     * @throws \Exception
     *
     */
    public function destroy($id)
    {
        $estrategia = Estrategias::find($id);
        if (empty($estrategia)) {
            Flash::error('Estrategia no encontrada');

            return redirect(route('estrategias.index'));
        }

        //dd($estrategia);

        $estrategia->delete($id);

        Flash::success('Estrategia eliminada correctamente.');

        return redirect(route('estrategias.index'));
    }

    public function finalizo($id)
    {
        $estrategia = Estrategias::find($id);
        if (empty($estrategia)) {
            Flash::error('Estrategia no encontrada');

            return redirect(route('estrategias.index'));
        }
        //dd($estrategia);
        $estrategia->listo = '1';

        $estrategia->save();


        Flash::success('Estrategia finalizada correctamente.');

        return redirect(route('estrategias.index'));
    }
}
