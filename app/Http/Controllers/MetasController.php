<?php

namespace App\Http\Controllers;

use DB;
use App\Quotation;

use App\Http\Requests\CreateMetasRequest;
use App\Http\Requests\UpdateMetasRequest;
use App\Repositories\MetasRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class MetasController extends AppBaseController
{
    /** @var  MetasRepository */
    private $metasRepository;

    public function __construct(MetasRepository $metasRepo)
    {
        $this->metasRepository = $metasRepo;
    }

    public function index(Request $request)
    {
        $metas = $this->metasRepository->all();
        $objetivos = DB::table('objetivos')->select('id', 'objetivo')->distinct()->get();
        //dd($objetivos);
        /*$id = DB::table('metas')
            ->join('objetivos', 'metas.id_objetivo', '=', 'objetivos.id')
            ->select('objetivos.objetivo', 'objetivos.id')
            ->get();*/

        return view('metas.index')
            ->with('metas', $metas)
            ->with('objetivos', $objetivos);
    }

    public function create()
    {
        return view('metas.create');
    }

    /**
     * Store a newly created Metas in storage.
     *
     * @param CreateMetasRequest $request
     *
     * @return Response
     */
    public function store(CreateMetasRequest $request)
    {
        $input = $request->all();
        //dd($input);

        $metas = $this->metasRepository->create($input);

        Flash::success('Meta agregada con éxito.');

        return redirect(route('metas.index'));
    }

    /**
     * Display the specified Metas.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $metas = $this->metasRepository->find($id);

        if (empty($metas)) {
            Flash::error('Meta no encontrada');

            return redirect(route('metas.index'));
        }

        return view('metas.show')->with('metas', $metas);
    }

    /**
     * Show the form for editing the specified Metas.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $metas = $this->metasRepository->find($id);

        if (empty($metas)) {
            Flash::error('Meta no encontrada');

            return redirect(route('metas.index'));
        }

        return view('metas.edit')->with('metas', $metas);
    }

    /**
     * Update the specified Metas in storage.
     *
     * @param int $id
     * @param UpdateMetasRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMetasRequest $request)
    {
        $metas = $this->metasRepository->find($id);

        if (empty($metas)) {
            Flash::error('Meta no encontrada');

            return redirect(route('metas.index'));
        }

        $metas = $this->metasRepository->update($request->all(), $id);

        Flash::success('Meta actualizada exitosamente.');

        return redirect(route('metas.index'));
    }

    /**
     * Remove the specified Metas from storage.
     *
     * @param int $id
     *
     * @return Response
     * @throws \Exception
     *
     */
    public function destroy($id)
    {
        $metas = $this->metasRepository->find($id);

        if (empty($metas)) {
            Flash::error('Meta no encontrada');

            return redirect(route('metas.index'));
        }

        $this->metasRepository->delete($id);

        Flash::success('Meta eliminada con éxito.');

        return redirect(route('metas.index'));
    }


}
