<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateObjetivosRequest;
use App\Http\Requests\UpdateObjetivosRequest;
use App\Repositories\ObjetivosRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class ObjetivosController extends AppBaseController
{
    /** @var  ObjetivosRepository */
    private $objetivosRepository;

    public function __construct(ObjetivosRepository $objetivosRepo)
    {
        $this->objetivosRepository = $objetivosRepo;
    }

    /**
     * Display a listing of the Objetivos.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $objetivos = $this->objetivosRepository->all();
        $array = array();
        foreach ($objetivos as $objetivo) {
            $valores =[
                'id' => $objetivo->id,
                'objetivo' => $objetivo->objetivo,
                'descripcion' => $objetivo->descripcion,
                'tipo' => $objetivo->tipo,
                'kpi' => $objetivo->kpi,
                'porcentaje' => $this->objetivosRepository->datos($objetivo->id),
                'est' => $this->objetivosRepository->estrategia($objetivo->id),
            ];
           array_push ($array, $valores);
        }
        return view('objetivos.index')
            ->with('objetivos', $array);
    }

    /**
     * Show the form for creating a new Objetivos.
     *
     * @return Response
     */
    public function create()
    {
        return view('objetivos.create');
    }

    /**
     * Store a newly created Objetivos in storage.
     *
     * @param CreateObjetivosRequest $request
     *
     * @return Response
     */
    public function store()
    {
        $input = \request()->all();
        $objetivos = $this->objetivosRepository->create($input);

        Flash::success('Objetivo almacenado correctamente.');

        return redirect(route('objetivos.index'));
    }

    /**
     * Display the specified Objetivos.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $objetivos = $this->objetivosRepository->find($id);

        if (empty($objetivos)) {
            Flash::error('Objetivo no encontrado');

            return redirect(route('objetivos.index'));
        }

        return view('objetivos.show')->with('objetivos', $objetivos);
    }

    /**
     * Show the form for editing the specified Objetivos.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $objetivos = $this->objetivosRepository->find($id);

        if (empty($objetivos)) {
            Flash::error('Objetivo no encontrado');

            return redirect(route('objetivos.index'));
        }

        return view('objetivos.edit')->with('objetivos', $objetivos);
    }

    /**
     * Update the specified Objetivos in storage.
     *
     * @param int $id
     * @param UpdateObjetivosRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateObjetivosRequest $request)
    {
        $objetivos = $this->objetivosRepository->find($id);

        if (empty($objetivos)) {
            Flash::error('Objetivo no encontrado');

            return redirect(route('objetivos.index'));
        }

        $objetivos = $this->objetivosRepository->update($request->all(), $id);

        Flash::success('Objetivo actualizado correctamente.');

        return redirect(route('objetivos.index'));
    }

    /**
     * Remove the specified Objetivos from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $objetivos = $this->objetivosRepository->find($id);

        if (empty($objetivos)) {
            Flash::error('Objetivo no encontrado');

            return redirect(route('objetivos.index'));
        }

        $this->objetivosRepository->delete($id);

        Flash::success('Objetivo eliminado.');

        return redirect(route('objetivos.index'));
    }

    public function objetivo($id)
    {
        $objetivos = $this->objetivosRepository->find($id);

        if (empty($objetivos)) {
            Flash::error('Objetivo no encontrado');

            return redirect(route('metas.index'));
        }

        return view('objetivos.show')->with('objetivos', $objetivos);

    }

}
