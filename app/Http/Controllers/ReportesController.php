<?php

namespace App\Http\Controllers;

use App\Exports\ObjetivoExport;
use App\Repositories\ObjetivosRepository;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Concerns\FromCollection;
use App\Objetivos;
use DateTime;

class ReportesController extends Controller
{
    /** @var  ObjetivosRepository */
    private $objetivosRepository;

    public function __construct(ObjetivosRepository $objetivosRepo)
    {
        $this->objetivosRepository = $objetivosRepo;
    }

    /**
     * Display a listing of the Objetivos.
     *
     * @param Request $request
     *
     * @return Response
     */

    public function index(Request $request){
        return view('reportes.index');
    }

    public function export()
    {
        $dat = \request()->all();
        $inicio = new DateTime($dat['incio']);
        $fin = new DateTime($dat['fin']);
        $inicio->modify('-1 day');
        $fin->modify('+1 day');
        $objetivos = $this->objetivosRepository->dates($inicio,$fin);
        $array = array();
        foreach ($objetivos as $objetivo) {
            $valores =[
                'id' => $objetivo->id,
                'objetivo' => $objetivo->objetivo,
                'descripcion' => $objetivo->descripcion,
                'kpi' => $objetivo->kpi,
                'porcentaje' => $this->objetivosRepository->datos_R($objetivo->id),
                'est' => $this->objetivosRepository->est($objetivo->id),
            ];
            array_push ($array, $valores);
        }
        $datos = new ObjetivoExport($array);
        $nombre = 'objetivos_'.$dat['incio'].'_'.$dat['fin'].'.xlsx';
        return Excel::download($datos, $nombre);
    }


}
