<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

/**
 * Class Estrategias
 * @package App\Models
 * @version February 24, 2020, 7:18 pm UTC
 *
 * @property \App\Models\Objetivo idObj
 * @property integer id_obj
 * @property string estrategia
 * @property boolean listo
 * @property string coment
 * @property string incio
 * @property string fin
 */
class Estrategias extends Model
{
    use SoftDeletes;

    public $table = 'estrategia';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'id_obj',
        'estrategia',
        'listo',
        'coment',
        'incio',
        'fin'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'id_obj' => 'integer',
        'estrategia' => 'string',
        'listo' => 'boolean',
        'coment' => 'string',
        'incio' => 'date',
        'fin' => 'date'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id_obj' => 'required',
        'estrategia' => 'required',
        'coment' => 'required',
        'incio' => 'required',
        'fin' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function idObj()
    {
        return $this->belongsTo(\App\Models\Objetivo::class, 'id_obj');
    }
}
