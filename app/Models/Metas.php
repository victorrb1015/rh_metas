<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Metas
 * @package App\Models
 * @version February 24, 2020, 7:12 pm UTC
 *
 * @property \App\Models\Objetivo idObjetivo
 * @property integer id_objetivo
 * @property string meta
 * @property string descripcion
 * @property string area
 * @property string inicio
 * @property string fin
 */
class Metas extends Model
{
    use SoftDeletes;

    public $table = 'metas';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'id_objetivo',
        'meta',
        'descripcion',
        'area',
        'inicio',
        'fin'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'id_objetivo' => 'integer',
        'meta' => 'string',
        'descripcion' => 'string',
        'area' => 'string',
        'inicio' => 'date',
        'fin' => 'date'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'meta' => 'required',
        'descripcion' => 'required',
        'area' => 'required',
        'inicio' => 'required',
        'fin' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function idObjetivo()
    {
        return $this->belongsTo(\App\Models\Objetivo::class, 'id_objetivo');
    }
}
