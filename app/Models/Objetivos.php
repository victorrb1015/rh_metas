<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Objetivos
 * @package App\Models
 * @version March 12, 2020, 10:24 pm UTC
 *
 * @property \App\Models\User idUser
 * @property \Illuminate\Database\Eloquent\Collection estrategia
 * @property \Illuminate\Database\Eloquent\Collection metas
 * @property integer id_user
 * @property string objetivo
 * @property string descripcion
 * @property boolean tipo
 * @property integer kpi
 */
class Objetivos extends Model
{
    use SoftDeletes;

    public $table = 'objetivos';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'id_user',
        'objetivo',
        'descripcion',
        'tipo',
        'kpi'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'id_user' => 'integer',
        'objetivo' => 'string',
        'descripcion' => 'string',
        'tipo' => 'boolean',
        'kpi' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id_user' => 'required',
        'objetivo' => 'required',
        'descripcion' => 'required',
        'tipo' => 'required',
        'kpi' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function idUser()
    {
        return $this->belongsTo(\App\Models\User::class, 'id_user');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function estrategia()
    {
        return $this->hasMany(\App\Models\Estrategium::class, 'id_obj');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function metas()
    {
        return $this->hasMany(\App\Models\Meta::class, 'id_objetivo');
    }
}
