<?php

namespace App\Repositories;

use App\Models\Estrategias;
use App\Models\Objetivos;
use App\Repositories\BaseRepository;

/**
 * Class EstrategiasRepository
 * @package App\Repositories
 * @version February 24, 2020, 7:18 pm UTC
*/

class EstrategiasRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_obj',
        'estrategia',
        'listo',
        'coment',
        'incio',
        'fin'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Estrategias::class;
    }

    public function  objetivos()
    {
        return Objetivos::get();
    }
}
