<?php

namespace App\Repositories;

use App\Models\Metas;
use App\Repositories\BaseRepository;

/**
 * Class MetasRepository
 * @package App\Repositories
 * @version February 24, 2020, 7:12 pm UTC
*/

class MetasRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_objetivo',
        'meta',
        'descripcion',
        'area',
        'inicio',
        'fin'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Metas::class;
    }
}
