<?php

namespace App\Repositories;

use App\Models\Estrategias;
use App\Models\Objetivos;
use DateTime;

/**
 * Class ObjetivosRepository
 * @package App\Repositories
 * @version March 12, 2020, 10:24 pm UTC
 */
class ObjetivosRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_user',
        'objetivo',
        'descripcion',
        'tipo',
        'kpi'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Objetivos::class;
    }

    public function datos($id)
    {
        $val1 = Estrategias::where('id_obj', '=', $id)
            ->where('listo', '=', '1')
            ->count();
        $val2 = Estrategias::where('id_obj', '=', $id)
            ->count();
        if ($val2 == 0) {
            $porcentaje = 0;
        } else {
            $porcentaje = round((($val1 * 100) / $val2), 2);
        }
        return $porcentaje;
    }
    public function datos_R($id)
    {
        $val1 = Estrategias::where('id_obj', '=', $id)
            ->where('listo', '=', '1')
            ->count();
        $val2 = Estrategias::where('id_obj', '=', $id)
            ->count();
        if ($val2 == 0) {
            $porcentaje = 0;
        } else {
            $porcentaje = round(($val1 / $val2), 4);
        }
        return $porcentaje;
    }

    public function estrategia($id)
    {
        return Estrategias::where('id_obj','=',$id)->get();
    }

    public function est($id)
    {
        return Estrategias::where('id_obj','=',$id)->count();
    }

    public function id()
    {
        $objetivos = Objetivos::select('id')->get();
        return $objetivos;
    }

    /**
     * @param DateTime $inicio
     * @param Datetime $fin
     * @return
     */
    public function dates(DateTime $inicio, Datetime $fin)
    {
        return Objetivos::whereBetween('created_at', [($inicio->format('Y-m-d') . ' 23:59:00'), ($fin->format('Y-m-d') . ' 00:00:00')])->get();
    }

}
