<?php

namespace App\Repositories;

use App\Models\Perfil;
use App\Models\User;
use App\Repositories\BaseRepository;

/**
 * Class UserRepository
 * @package App\Repositories
 * @version February 26, 2020, 5:16 pm UTC
*/

class UserRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'email',
        'password',
        'id_perfil'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return User::class;
    }

    public function perfiles()
    {
        return Perfil::all();
    }

    public function index()
    {
        $user = User::join('perfiles','perfiles.id','=','users.id_perfil')
                ->select('users.*','perfiles.perfil')
                ->get();
        return $user;
    }
}
