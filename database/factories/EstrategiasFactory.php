<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Estrategias;
use Faker\Generator as Faker;

$factory->define(Estrategias::class, function (Faker $faker) {

    return [
        'id_obj' => $faker->word,
        'estrategia' => $faker->word,
        'listo' => $faker->word,
        'coment' => $faker->text,
        'incio' => $faker->word,
        'fin' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
