<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Metas;
use Faker\Generator as Faker;

$factory->define(Metas::class, function (Faker $faker) {

    return [
        'id_objetivo' => $faker->word,
        'meta' => $faker->word,
        'descripcion' => $faker->text,
        'area' => $faker->word,
        'inicio' => $faker->word,
        'fin' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
