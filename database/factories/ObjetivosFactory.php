<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Objetivos;
use Faker\Generator as Faker;

$factory->define(Objetivos::class, function (Faker $faker) {

    return [
        'id_user' => $faker->word,
        'objetivo' => $faker->word,
        'descripcion' => $faker->text,
        'tipo' => $faker->word,
        'kpi' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
