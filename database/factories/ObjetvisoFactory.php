<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Objetviso;
use Faker\Generator as Faker;

$factory->define(Objetviso::class, function (Faker $faker) {

    return [
        'id_user' => $faker->word,
        'objetivo' => $faker->word,
        'descripcion' => $faker->text,
        'kpi_meta' => $faker->randomDigitNotNull,
        'kpi' => $faker->randomDigitNotNull,
        'herramienta' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
