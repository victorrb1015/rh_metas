<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });

        schema::create('objetivos', function (Blueprint $table){
            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_user');
            $table->foreign('id_user')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->string('objetivo');
            $table->text('descripcion');
            $table->boolean('tipo');
            $table->integer('kpi');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('metas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_objetivo');
            $table->foreign('id_objetivo')
                ->references('id')
                ->on('objetivos')
                ->onDelete('cascade');
            $table->string('meta');
            $table->text('descripcion');
            $table->string('area');
            $table->date('inicio');
            $table->date('fin');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('estrategia', function (Blueprint $table){
            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_obj');
            $table->foreign('id_obj')
                ->references('id')
                ->on('objetivos')
                ->onDelete('cascade');
            $table->string('estrategia');
            $table->boolean('listo')->default(false);
            $table->text('coment')->default(null);
            $table->date('incio');
            $table->date('fin');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('metas');
        Schema::dropIfExists('estrategia');
        Schema::dropIfExists('objetivos');
        Schema::dropIfExists('users');
    }
}
