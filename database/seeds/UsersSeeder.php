<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('perfiles')->insert([
            'perfil' => 'Desarrollador'
        ]);
        DB::table('users')->insert([
            'name'=>'Victor Rojas',
            'email'=> 'vic@gmail.com',
            'password'=> Hash::make('123456789'),
            'id_perfil' => 1
        ]);
        DB::table('users')->insert([
            'name'=>'John Vargas',
            'email'=> 'John@gmail.com',
            'password'=> Hash::make('Administrador1'),
            'id_perfil' => 1
        ]);
    }
}
