@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Estrategias
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($estrategias, ['route' => ['estrategias.update', $estrategias->id], 'method' => 'patch']) !!}

                        @include('estrategias.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection