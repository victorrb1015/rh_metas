<div class="md-form">
    <select class="mdb-select md-form colorful-select dropdown-default" name="id_obj" required>
        <option value="" disabled selected>Objetivo</option>
        @foreach($objetivos as $p)
            <option value="{{$p->id}}">{{$p->objetivo}}</option>
        @endforeach
    </select>
</div>

<!-- Estrategia Field -->
<div class="md-form">
    {!! Form::label('estrategia', 'Estrategia:') !!}
    {!! Form::text('estrategia', null, ['class' => 'form-control']) !!}
</div>


<!-- Coment Field -->
<div class="md-form">
    {!! Form::label('coment', 'Comentario:') !!}
    {!! Form::textarea('coment', null, ['class' => 'form-control']) !!}
</div>

<!-- Incio Field -->
<div class="md-form input-group">
    <div>
        <input placeholder="Inicio" type="text" name="incio" class="form-control datepicker">
    </div>
    <div>
        <input placeholder="Fin" type="text" name="fin" class="form-control datepicker">
    </div>
</div>

{!! Form::hidden('listo',0)!!}


@push('scripts')
    $('.datepicker').pickadate({
    monthsFull: ['enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre'],
    monthsShort: ['ene', 'feb', 'mar', 'abr', 'may', 'jun', 'jul', 'ago', 'sep', 'oct', 'nov', 'dic'],
    weekdaysFull: ['domingo', 'lunes', 'martes', 'miércoles', 'jueves', 'viernes', 'sábado'],
    weekdaysShort: ['dom', 'lun', 'mar', 'mié', 'jue', 'vie', 'sáb'],
    today: '',
    clear: 'Borrar',
    close: 'Cerrar',
    firstDay: 1,
    format: 'yyyy-mm-dd',
    formatSubmit: 'dd-mm-yyyy'
    });
@endpush


