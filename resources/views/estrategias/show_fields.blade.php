<!-- Id Obj Field -->
<div class="form-group">
    {!! Form::label('id_obj', 'Id Obj:') !!}
    <p>{{ $estrategias->id_obj }}</p>
</div>

<!-- Estrategia Field -->
<div class="form-group">
    {!! Form::label('estrategia', 'Estrategia:') !!}
    <p>{{ $estrategias->estrategia }}</p>
</div>

<!-- Listo Field -->
<div class="form-group">
    {!! Form::label('listo', 'Listo:') !!}
    <p>{{ $estrategias->listo }}</p>
</div>

<!-- Coment Field -->
<div class="form-group">
    {!! Form::label('coment', 'Coment:') !!}
    <p>{{ $estrategias->coment }}</p>
</div>

<!-- Incio Field -->
<div class="form-group">
    {!! Form::label('incio', 'Incio:') !!}
    <p>{{ $estrategias->incio }}</p>
</div>

<!-- Fin Field -->
<div class="form-group">
    {!! Form::label('fin', 'Fin:') !!}
    <p>{{ $estrategias->fin }}</p>
</div>

