<div class="table-responsive">
    <div class="form-group pull-right">
        <input type="text" class="search form-control" placeholder="¿Qué estas buscando?">
    </div>
    <table class="table table-hover table-bordered results responsive-table text-center">
        <thead>
            <tr>
                <th>Objetivo</th>
                <th>Estrategia</th>
                <th>Comentario</th>
                <th colspan="2">Fechas</th>
                <th>Finalizada</th>
                <th>Editar</th>
                <!--<th>Eliminar</th>-->
            </tr>
            <tr class="warning no-result">
                <td colspan="14" style="color:red; font-size: 20px;"><i class="fa fa-warning"></i> No se encontro
                    registro con la información ingresada
                </td>
            </tr>
        </thead>
        <tbody>
        @foreach($estrategia as $estrategias)
            <tr>
                <td>{{ $estrategias->objetivo }}</td>
                <td>{{ $estrategias->estrategia }}</td>
                <td>{{ $estrategias->coment }}</td>
                <td>{{ date('d/m/yy', strtotime($estrategias->incio)) }}</td>
                <td>{{ date('d/m/yy', strtotime($estrategias->fin ))}}</td>
                <td>@if($estrategias->listo)
                        <a type="button" class="btn-floating btn-sm success-color"><i class="fas fa-check"></i></a>
                        @else
                        <a class="btn-floating btn-sm btn-danger" href="{{route('finalizo',$estrategias->id)}}" id="btn-one"><i class="fas fa-times"></i></a>
                    @endif
                </td>

                    <td>
                        <a  data-toggle="modal"  data-target="#EditarModal{{$estrategias->id}}"class='btn btn-default btn-sm white-text'><i class="far fa-edit"></i></a>
                    <!--
                            <button type="button" class="btn btn-primary btn-sm white-text" data-toggle="modal"
                                    data-target="#EditarModal{{$estrategias->id}}">
                                <i class="far fa-edit"></i>
                            </button>

                            Modal -->
                            <div class="modal fade" id="EditarModal{{$estrategias->id}}" tabindex="-1" role="dialog"
                                 aria-labelledby="EditarModalexm"
                                 aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="EditarModalexm">{{$estrategias->estrategia}}</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        {!! Form::model($estrategias, ['route' => ['estrategias.update', $estrategias->id], 'method' => 'patch']) !!}
                                        <div class="modal-body">
                                            @include('estrategias.fields')
                                        </div>
                                        <div class="modal-footer">
                                            <!-- Submit Field -->
                                            {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                        </div>
                                        {!! Form::close() !!}
                                        <div class="modal-footer">

                                        </div>
                                    </div>
                                </div>
                            </div>

                    </td>
                <!--<td>
                {!! Form::open(['route' => ['estrategias.destroy', $estrategias->id], 'method' => 'delete']) !!}
                        {!! Form::button('<i class="fas fa-times-circle"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-sm', 'onclick' => "return confirm('¿Estas seguro?')"]) !!}
                {!! Form::close() !!}
                </td>-->

            </tr>
        @endforeach
        </tbody>
    </table>
</div>
<br>
