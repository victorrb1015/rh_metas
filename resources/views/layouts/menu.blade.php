<li class="{{ Request::is('metas*') ? 'active' : '' }}">
    <a href="{{ route('metas.index') }}"><i class="fa fa-edit"></i><span>Metas</span></a>
</li>

<li class="{{ Request::is('estrategias*') ? 'active' : '' }}">
    <a href="{{ route('estrategias.index') }}"><i class="fa fa-edit"></i><span>Estrategias</span></a>
</li>

<li class="{{ Request::is('objetivos*') ? 'active' : '' }}">
    <a href="{{ route('objetivos.index') }}"><i class="fa fa-edit"></i><span>Objetivos</span></a>
</li>

<li class="{{ Request::is('users*') ? 'active' : '' }}">
    <a href="{{ route('users.index') }}"><i class="fa fa-edit"></i><span>Users</span></a>
</li>

<li class="{{ Request::is('perfils*') ? 'active' : '' }}">
    <a href="{{ route('perfils.index') }}"><i class="fa fa-edit"></i><span>Perfils</span></a>
</li>

<li class="{{ Request::is('objetvisos*') ? 'active' : '' }}">
    <a href="{{ route('objetvisos.index') }}"><i class="fa fa-edit"></i><span>Objetvisos</span></a>
</li>

