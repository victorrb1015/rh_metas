<nav class="navbar navbar-expand-lg navbar-dark fixed-top scrolling-navbar black justify-content-between">
    <div class="container smooth-scroll">
        <a class="navbar-brand " href="{{url('/')}}"><img src="{{asset('logo/logo/login-logo.png')}}" style="width: 40px;"></a>

        <button class="navbar-toggler" type="button" data-toggle="collapse"
                data-target="#navbarSupportedContent-7" aria-controls="navbarSupportedContent-7"
                aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent-4">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item dropdown">
                    <a type="button" class=" nav-link btn btn-default btn-sm" id="navbarDropdownMenuLink-4"
                       aria-haspopup="true" aria-expanded="false" data-toggle="modal" data-target="#Modal">Menu</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle " id="navbarDropdownMenuLink-4" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-user"></i>{{ Auth::user()->name }} </a>
                    <div class="dropdown-menu dropdown-menu-right dropdown-default" aria-labelledby="navbarDropdownMenuLink-4">
                        <!--<a class="dropdown-item" href="#">Mi cuenta</a>-->
                        <a class="dropdown-item" type="#logout-form" onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">Salir</a>
                    </div>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li>
            </ul>
        </div>
    </div>
</nav>

<!-- Frame Modal Bottom -->
<div class="modal fade top" id="Modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">

    <!-- Add class .modal-frame and then add class .modal-bottom (or other classes from list above) to set a position to the modal -->
    <div class="modal-dialog modal-frame modal-top" role="document">


        <div class="modal-content">
            <div class="modal-body">
                <div class="row d-flex justify-content-center align-items-center">
                    <a type="button" class="btn btn-default btn-rounded" href="{{url('metas')}}"><i class="fas fa-desktop"></i> Metas</a>
                    <a type="button" class="btn btn-default btn-rounded" href="{{url('objetivos')}}"><i class="fas fa-tasks"></i> Objetivos</a>
                    <a type="button" class="btn btn-default btn-rounded" href="{{url('estrategias')}}"><i class="far fa-building"></i> Estrategias</a>
                    <a type="button" class="btn btn-default btn-rounded" href="{{url('reportes')}}"><i class="far fa-chart-bar"></i> Reportes</a>
                    <a type="button" class="btn btn-default btn-rounded" href="{{url('users')}}"><i class="fas fa-user"></i> Usuarios</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Frame Modal Bottom -->

