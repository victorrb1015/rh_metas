<div class="md-form input-group">
        {!! Form::text('meta', null, ['class' => 'form-control', 'id' => 'meta', 'placeholder' => 'Meta', 'required']) !!}
    <select class="browser-default custom-select" name="area" required>
        <option value="" disabled selected>Aréa</option>
        <option value="Sistemas">Sistemas</option>
        <option value="RH">Recursos Humanos</option>
        <option value="CMR">CMR</option>
    </select>
</div>

<!-- Descripcion Field -->
<div class="form-group col-sm-12 col-lg-12">
    <select class="browser-default custom-select center-align" name="id_objetivo" required>
        <option value="" disabled selected>¿Se relaciona a un objetivo?</option>
        <option value="no">No tiene objetivo</option>
        @foreach($objetivos as $item)
        <option value="{{$item->id}}">{{$item->objetivo}}</option>
            @endforeach
    </select>
</div>

<!-- Descripcion Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('descripcion', 'Descripción:') !!}
    {!! Form::textarea('descripcion', null, ['class' => 'form-control', 'required']) !!}
</div>

<div class="md-form input-group">
    <span style="font-size: 12px;">Fecha inicio:</span>
    {!! Form::date('inicio', null, ['class' => 'form-control','id'=>'inicio', 'required']) !!}
    <span style="font-size: 12px;">Fecha Final:</span>
    <!-- Fin Field -->
    {!! Form::date('fin', null, ['class' => 'form-control','id'=>'fin', 'required']) !!}

</div>

@section('scripts')
    <script type="text/javascript">
        $('#inicio').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endsection


@section('scripts')
    <script type="text/javascript">
        $('#fin').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endsection


