@extends('layouts.app')
@section('title', 'Metas')
@section('content')
    <section class="content">
        <div class="card card-cascade wilder">
            <!-- Card image -->
            <div class="view view-cascade gradient-card-header default-color">
                <!-- Title -->
                <h3 class="card-header-title">Metas</h3>
            </div>
        </div>
        <hr>
        @include('layouts.errors')
        @include('flash::message')
        <div class="row">
            <div class="col-8">
                <a type="button" class="btn btn-default btn-lg" data-toggle="modal" data-target="#buscar_factura">
                    Metas encontradas:
                </a>
                <!-- Modal Busqueda-->
                <!--<div class="modal fade" id="buscar_factura" tabindex="-1" role="dialog"
                     aria-labelledby="exampleModalLabel"
                     aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Buscar Meta</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>

                            <div class="modal-body">
                            @include('layouts.errors')
                                <div class="row">
                                    <div class="col-md-9 col-lg-10">
                                        <div class="md-form">
                                            <i class="fas fa-clipboard-list prefix"></i>
                                            <input type="text" id="rs" name="rs" class="form-control">
                                            <label for="rs">Meta</label>
                                        </div>
                                        <div class="md-form">
                                            <i class="fas fa-address-card prefix"></i>
                                            <input type="text" id="rfc" name="rfc" class="form-control">
                                            <label for="rfc">Aréa</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar
                                </button>
                                {!! Form::submit('Enviar', ['class' => 'btn btn-primary', 'id' => 'enviar']) !!}
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>-->
                <!--termina modal busqueda-->
            </div>
            <div class="col-4 text-right">
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-dark btn-rounded" data-toggle="modal"
                        data-target="#basicExampleModal">
                    Añadir meta
                </button>
            </div>
        </div>
        <br>
        @include('metas.table')
    </section>
    <br>

<!-- Modal añadir-->
<div class="modal fade" id="basicExampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Añadir meta</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {!! Form::open(['route' => 'metas.store']) !!}
            <div class="modal-body">

                @include('metas.fields')

            </div>
            <div class="modal-footer">
                <!-- Submit Field -->
                {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<!--termina modal-->
@endsection

<script>
    @push('scripts')
    $(document).ready(function () {
        $('.mdb-select').materialSelect();
    });

    @endpush
</script>


