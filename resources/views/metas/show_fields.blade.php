<!-- Meta Field -->
<div class="form-group">
    {!! Form::label('meta', 'Meta:') !!}
    <p>{{ $metas->meta }}</p>
</div>

<!-- Descripcion Field -->
<div class="form-group">
    {!! Form::label('descripcion', 'Descripcion:') !!}
    <p>{{ $metas->descripcion }}</p>
</div>

<!-- Area Field -->
<div class="form-group">
    {!! Form::label('area', 'Area:') !!}
    <p>{{ $metas->area }}</p>
</div>

<!-- Inicio Field -->
<div class="form-group">
    {!! Form::label('inicio', 'Inicio:') !!}
    <p>{{ $metas->inicio }}</p>
</div>

<!-- Fin Field -->
<div class="form-group">
    {!! Form::label('fin', 'Fin:') !!}
    <p>{{ $metas->fin }}</p>
</div>

