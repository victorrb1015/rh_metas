<div class="table-responsive">
    <div class="form-group pull-right">
        <input type="text" class="search form-control" placeholder="¿Qué estas buscando?">
    </div>
    <table class="table table-hover table-bordered results responsive-table text-center">
        <thead>
        <tr>
            <th>Meta</th>
            <th>Descripción</th>
            <th>Aréa</th>
            <th>Fecha inicio - Fecha Final</th>
            <th>Objetivos</th>
            <th>Editar</th>
            <th>Eliminar</th>
        </tr>
        <tr class="warning no-result">
            <td colspan="14" style="color:red; font-size: 20px;"><i class="fa fa-warning"></i> No se encontro
                registro con la información ingresada
            </td>
        </tr>
        </thead>
        <tbody>
        @foreach($metas as $metas)
            <tr>
                <td>{!! $metas->meta !!}</td>
                <td>
                    <button type="button" class="btn btn-sm btn-default" data-toggle="modal"
                            data-target="#description{{$metas->id}}">
                        <i class="fas fa-info"></i>
                    </button>

                    <!-- Modal -->
                    <div class="modal fade" id="description{{$metas->id}}" tabindex="-1" role="dialog"
                         aria-labelledby="exampleModalLabel"
                         aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Descripción</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <p style="font-size: 22px;">{{ $metas->descripcion }}</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </td>
                <td>{{ $metas->area }}</td>
                <td>{{ $metas->inicio->format('d/m/Y')}} - {{ $metas->fin->format('d/m/Y') }}</td>
                <td>
                    <a href="{{ route('objetivos', [$metas->id_objetivo]) }}" class='btn btn-primary btn-sm'><i class="far fa-eye white-text"></i></a>
                </td>
                <td>
                    <div class='btn-group'>
                        <a type="button" class="btn btn-default btn-sm" data-toggle="modal"
                           data-target="#UpdateModal{{$metas->id}}" href="{{ route('metas.edit', [$metas->id]) }}">
                            <i class="fas fa-edit white-text"></i>
                        </a>
                        <!-- Modal -->
                        {!! Form::model($metas, ['route' => ['metas.update', $metas->id], 'method' => 'patch']) !!}
                        <div class="modal fade" id="UpdateModal{{$metas->id}}" tabindex="-1" role="dialog"
                             aria-labelledby="exampleModalLabel"
                             aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title center-align" id="exampleModalLabel">Añadir meta</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>

                                    <div class="modal-body">
                                        <div class="md-form input-group">
                                            {!! Form::text('meta', $metas->meta, ['class' => 'form-control', 'id' => 'meta', 'placeholder' => 'Meta', 'required']) !!}
                                            <select class="browser-default custom-select" name="area" required>
                                                <option value="" disabled selected>Aréa</option>
                                                <option value="Sistemas">Sistemas</option>
                                                <option value="RH">Recursos Humanos</option>
                                                <option value="CMR">CMR</option>
                                            </select>
                                        </div>

                                        <!-- Descripcion Field -->
                                        <div class="form-group col-sm-12 col-lg-12">
                                            {!! Form::label('descripcion', 'Descripcion:') !!}
                                            {!! Form::textarea('descripcion', $metas->descripcion, ['class' => 'form-control']) !!}
                                        </div>

                                        <div class="md-form input-group">
                                            <span style="font-size: 12px;">Fecha inicio:</span>
                                            {!! Form::date('inicio', $metas->inicio, ['class' => 'form-control','id'=>'inicio', 'required']) !!}
                                            <span style="font-size: 12px;">Fecha Final:</span>
                                            <!-- Fin Field -->
                                            {!! Form::date('fin', $metas->fin, ['class' => 'form-control','id'=>'fin', 'required']) !!}

                                        </div>

                                    </div>
                                    <div class="modal-footer">
                                        {!! Form::submit('Actualizar', ['class' => 'btn btn-default']) !!}
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--modal-->
                        {!! Form::close() !!}
                    </div>
                </td>
                <td>
                    {!! Form::open(['route' => ['metas.destroy', $metas->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>

                        {!! Form::button('<i class="far fa-times-circle white-text"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-sm', 'onclick' => "return confirm('¿Estas seguro?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

<script>
    //select
    @push('scripts')
    $(document).ready(function () {
        $('.mdb-select').materialSelect();
    });

    //datepickers
    $('#inicio').datetimepicker({
        format: 'YYYY-MM-DD HH:mm:ss',
        useCurrent: false
    });

    $('#fin').datetimepicker({
        format: 'YYYY-MM-DD HH:mm:ss',
        useCurrent: false
    });
    @endpush
</script>


