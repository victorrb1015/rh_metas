@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Objetivos
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($objetivos, ['route' => ['objetivos.update', $objetivos->id], 'method' => 'patch']) !!}

                        @include('objetivos.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection