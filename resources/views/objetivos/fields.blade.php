{!! Form::hidden('id_user', \Illuminate\Support\Facades\Auth::id(), ['class' => 'form-control']) !!}
<div class="md-form input-group">
    {!! Form::text('objetivo', null, ['class' => 'form-control', 'id' => 'objetivo', 'placeholder' => 'Objetivo', 'required']) !!}

    {!! Form::number('kpi_meta', null, ['class' => 'form-control', 'id' => 'kpi_meta','name' => 'kpi' ,'placeholder' => 'Kpi de este año', 'required']) !!}
    <!-- Material unchecked -->
        <select class="browser-default custom-select" name="tipo" required>
            <option value="" disabled selected>¿Es porcentaje?</option>
            <option value="1">si</option>
            <option value="0">no</option>
        </select>
</div>

<!-- Descripcion Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('descripcion', 'Descripción:') !!}
    {!! Form::textarea('descripcion', null, ['class' => 'form-control', 'required']) !!}
</div>

<div class="md-form input-group">
    <span style="font-size: 12px;">Fecha inicio:</span>
    {!! Form::date('inicio', null, ['class' => 'form-control','id'=>'inicio', 'required']) !!}
    <span style="font-size: 12px;">Fecha Final:</span>
    <!-- Fin Field -->
    {!! Form::date('fin', null, ['class' => 'form-control','id'=>'fin', 'required']) !!}

</div>

@section('scripts')
    <script type="text/javascript">
        $('#inicio').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endsection


@section('scripts')
    <script type="text/javascript">
        $('#fin').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endsection

@push('scrpits')
    $(document).ready(function() {
    $('.mdb-select').materialSelect();
    });
@endpush
