@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Objetivos encontrados
        </h1>
    </section>
    <br>
    <div class="content">
        @include('objetivos.show_fields')
        <a href="{{ route('metas.index') }}" class="btn btn-default">Volver</a>
    </div>

@endsection
