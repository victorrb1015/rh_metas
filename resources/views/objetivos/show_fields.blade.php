<!-- Id User Field -->
<!--<div class="form-group">
    {!! Form::label('id_user', 'Id User:') !!}
    <p>{{ $objetivos->id_user }}</p>
</div>-->

<!-- Objetivo Field -->
<!--<div class="form-group">
    {!! Form::label('objetivo', 'Objetivo:') !!}
    <p>{{ $objetivos->objetivo }}</p>
</div>-->

<!-- Descripcion Field -->
<!--<div class="form-group">
    {!! Form::label('descripcion', 'Descripción:') !!}
    <p>{{ $objetivos->descripcion }}</p>
</div>-->

<!-- Tipo Field -->
<!--<div class="form-group">
    {!! Form::label('tipo', 'Tipo:') !!}
    <p>{{ $objetivos->tipo }}</p>
</div>-->

<!-- Kpi Field -->
<!--<div class="form-group">
    {!! Form::label('kpi', 'Kpi:') !!}
    <p>{{ $objetivos->kpi }}</p>
</div>-->

<table id="dtBasicExample" class="table table-hover table-bordered results responsive-table text-center" cellspacing="0"
       width="100%">
    <thead>
    <tr>
        <th class="th-sm">Objetivos
        </th>
        <th class="th-sm">Descripción
        </th>
        <th class="th-sm">KPI
        </th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>{{ $objetivos->objetivo }}</td>
        <td>{{ $objetivos->descripcion }}</td>
        <td>{{ $objetivos->kpi }}</td>
    </tr>

    </tbody>
    <!--<tfoot>
    <tr>
        <th>Name
        </th>
        <th>Position
        </th>
        <th>Office
        </th>
        <th>Age
        </th>
        <th>Start date
        </th>
        <th>Salary
        </th>
    </tr>
    </tfoot>-->
</table>
