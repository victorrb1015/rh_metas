<style>
    .md-v-line {
        position: absolute;
        border-left: 1px solid rgba(0,0,0,.125);
        height: 50px;
        top:0px;
        left:54px;
    }
</style>
<div class="table-responsive">
    <div class="form-group pull-right">
        <input type="text" class="search form-control" placeholder="¿Qué estas buscando?">
    </div>
    <table class="table table-hover table-bordered results responsive-table text-center">
        <thead>
        <tr>
            <th>Objetivo</th>
            <th>Descripcion</th>
            <th>Kpi meta</th>
            <td>Avance</td>
            <td>Estrategias</td>
            <th>Editar</th>
            <th>Eliminar</th>
        </tr>
        <tr class="warning no-result">
            <td colspan="14" style="color:red; font-size: 20px;"><i class="fa fa-warning"></i> No se encontro
                registro con la información ingresada
            </td>
        </tr>
        </thead>
        <tbody>
        @foreach($objetivos as $objetivos)
            <tr>
                <td>{{ $objetivos['objetivo'] }}</td>
                <td>{{ $objetivos['descripcion'] }}</td>
                <td>{{ $objetivos['kpi'] }}
                    @if($objetivos['tipo'])
                        %
                    @else
                    @endif
                </td>
                <td>
                    <div class="progress md-progress" style="height: 20px">
                        <div class="progress-bar morpheus-den-gradient" role="progressbar"
                             style="width: {{$objetivos['porcentaje']}}%; height: 20px"
                             aria-valuenow="{{$objetivos['porcentaje']}}" aria-valuemin="0"
                             aria-valuemax="100">{{$objetivos['porcentaje']}}%
                        </div>
                    </div>
                </td>
                <td>
                    <a type="button" class="btn-floating btn-sm info-color" data-toggle="modal"
                       data-target="#EstrategiaModal{{$objetivos['id']}}"><i class="fab fa-flipboard"></i></a>

                    <div class="modal fade" id="EstrategiaModal{{$objetivos['id']}}" tabindex="-1" role="dialog"
                         aria-labelledby="EditarModalexm"
                         aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="EditarModalexm"> Estrategias </h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <ul class="list-group text-left">
                                        @forelse($objetivos['est'] as $est)
                                            <li class="list-group-item">
                                                <div class="md-v-line"></div>
                                                @if($est->listo)
                                                    <i class="fas fa-check mr-4 pr-3 green-text"></i>
                                                    @else
                                                    <i class="far fa-times-circle mr-4 pr-3 red-text"></i>
                                                @endif
                                                {{$est->estrategia}}
                                            </li>
                                        @empty
                                            Sin Estrategias
                                        @endforelse
                                        </ul>
                                </div>
                                <div class="modal-footer">
                                    <!-- Submit Field -->
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                </div>
                                <div class="modal-footer">

                                </div>
                            </div>
                        </div>
                    </div>

                </td>


            <!--<td>
                    <a href="{{ route('objetivos.show', [$objetivos['id']]) }}" class='btn btn-default btn-xs'><i
                            class="glyphicon glyphicon-eye-open"></i></a>
                </td>-->
                <td>
                <!--<a href="{{ route('objetivos.edit', [$objetivos['id']]) }}" id="btn-one" class="btn btn-primary btn-sm white-text"><i class="far fa-edit"></i></a>-->
                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-primary btn-sm white-text" data-toggle="modal"
                            data-target="#EditarModal{{$objetivos['id']}}">
                        <i class="far fa-edit"></i>
                    </button>

                    <!-- Modal -->
                    <div class="modal fade" id="EditarModal{{$objetivos['id']}}" tabindex="-1" role="dialog"
                         aria-labelledby="EditarModalexm"
                         aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="EditarModalexm">{{$objetivos['objetivo']}}</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                {!! Form::model($objetivos, ['route' => ['objetivos.update', $objetivos['id']], 'method' => 'patch']) !!}
                                <div class="modal-body">
                                    @include('objetivos.fields')
                                </div>
                                <div class="modal-footer">
                                    <!-- Submit Field -->
                                    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                </div>
                                {!! Form::close() !!}
                                <div class="modal-footer">

                                </div>
                            </div>
                        </div>
                    </div>

                </td>
                {!! Form::open(['route' => ['objetivos.destroy', $objetivos['id']], 'method' => 'delete']) !!}
                <td>
                    {!! Form::button('<i class="fas fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-sm', 'onclick' => "return confirm('¿Estas seguro?')"]) !!}
                </td>
                {!! Form::close() !!}
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

