@extends('layouts.app')
@section('title', 'Perfiles')
@section('content')
    <section class="content">
        <div class="card card-cascade wilder">
            <!-- Card image -->
            <div class="view view-cascade gradient-card-header default-color">
                <!-- Title -->
                <h3 class="card-header-title">Perfiles</h3>
            </div>
        </div>
        <hr>
        @include('layouts.errors')
        @include('flash::message')
        <div class="row">
            <div class="col-8">
                <a type="button" class="btn btn-default btn-rounded" href="{{url('/users')}}">
                    Usuarios
                </a>
            </div>
            <div class="col-4 text-right">
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-dark btn-rounded" data-toggle="modal"
                        data-target="#basicExampleModal">
                    Añadir Perfil
                </button>
            </div>
        </div>
        <br>
        @include('perfils.table')
    </section>

    <!-- Modal añadir-->
    <div class="modal fade" id="basicExampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Añadir Perfil</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                {!! Form::open(['route' => 'perfils.store']) !!}
                <div class="modal-body">
                    @include('perfils.fields')
                </div>
                <div class="modal-footer">
                    <!-- Submit Field -->
                    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

<script>
    @push('scripts')
    $(document).ready(function () {
        $('.mdb-select').materialSelect();
    });

    @endpush
</script>

