<div class="table-responsive">
    <table class="table" id="perfils-table">
        <thead>
        <tr>
            <th>Perfil</th>
            <th>Editar</th>
            <th>Eliminar</th>
        </tr>
        </thead>
        <tbody>
        @foreach($perfils as $perfil)
            <tr>
                <td>{{ $perfil->perfil }}</td>
                <td>
                    <a data-toggle="modal"
                       data-target="#editar_{{$perfil->id}}" class='btn btn-default btn-xs'><i class="far fa-edit"></i></a>
                    <!-- Modal editar-->
                    <div class="modal fade" id="editar_{{$perfil->id}}" tabindex="-1" role="dialog"
                         aria-labelledby="exampleModalLabel"
                         aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Editar Perfil</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                {!! Form::model($perfil, ['route' => ['perfils.update', $perfil->id], 'method' => 'patch']) !!}
                                <div class="modal-body">
                                    @include('perfils.fields')
                                </div>
                                <div class="modal-footer">
                                    <!-- Submit Field -->
                                    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </td>
                <td>
                    {!! Form::open(['route' => ['perfils.destroy', $perfil->id], 'method' => 'delete']) !!}
                    {!! Form::button('<i class="fas fa-times-circle"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
