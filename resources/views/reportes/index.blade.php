@extends('layouts.app')
@section('title', 'Reportes')
@section('content')
    <section class="content">
        <div class="card card-cascade wilder">
            <!-- Card image -->
            <div class="view view-cascade gradient-card-header default-color">
                <!-- Title -->
                <h3 class="card-header-title">Reportes</h3>
            </div>
        </div>
        <hr>
        @include('layouts.errors')
        @include('flash::message')
        <div class="row">
            <div class="col-4">
            </div>
            <!-- Card -->
            <div class="card">
                                <!-- Card content -->
                <div class="card-body">
                    <!-- Title -->
                    <h4 class="card-title"><a>Reporte de Objetivos</a></h4>
                    <!-- Text -->
                {!! Form::open(['route' => 'descargaexcel', 'method' => 'post',  'enctype' => 'multipart/form-data']) !!}
                    <div class="md-form input-group">
                        <div>
                            <input placeholder="Inicio" type="text" name="incio" class="form-control datepicker">
                        </div>
                        <div>
                            <input placeholder="Fin" type="text" name="fin" class="form-control datepicker">
                        </div>
                    </div>
                {!! Form::submit('cargar excel', ['class' => 'btn btn-primary']) !!}
                {!! Form::close() !!}
                    <!-- Button -->
                </div>
            </div>
            <!-- Card -->
        </div>
    </section>

@endsection

<script>
    @push('scripts')
    $(document).ready(function () {
        $('.mdb-select').materialSelect();
    });
    $('.datepicker').pickadate({
        monthsFull: ['enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre'],
        monthsShort: ['ene', 'feb', 'mar', 'abr', 'may', 'jun', 'jul', 'ago', 'sep', 'oct', 'nov', 'dic'],
        weekdaysFull: ['domingo', 'lunes', 'martes', 'miércoles', 'jueves', 'viernes', 'sábado'],
        weekdaysShort: ['dom', 'lun', 'mar', 'mié', 'jue', 'vie', 'sáb'],
        today: '',
        clear: 'Borrar',
        close: 'Cerrar',
        firstDay: 1,
        format: 'yyyy-mm-dd',
        formatSubmit: 'dd-mm-yyyy'
    });
    @endpush
</script>
