<!-- Name Field -->
<div class="md-form">
    {!! Form::label('name', 'Nombre:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Field -->
<div class="md-form">
    {!! Form::label('email', 'Correo:') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<!-- Password Field -->
<div class="md-form">
    {!! Form::label('password', 'Contraseña:') !!}
    {!! Form::password('password', ['class' => 'form-control']) !!}
</div>

<div class="md-form">
    <select class="mdb-select md-form colorful-select dropdown-default" name="id_perfil" required>
        <option value="" disabled selected>Perfil</option>
        @foreach($perfiles as $p)
            <option value="{{$p->id}}">{{$p->perfil}}</option>
        @endforeach
    </select>
</div>
