<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::Routes();
Route::group(['middleware' => 'auth'],function (){
    Route::post('/metas', 'MetasController@create')->name('crear');
    Route::get('/meta/objetivo/{id}','ObjetivosController@objetivo')->name('objetivos');
    Route::get('/', 'HomeController@index')->middleware('verified');
    Route::resource('metas', 'MetasController');
    Route::resource('estrategias', 'EstrategiasController');
    Route::resource('objetivos', 'ObjetivosController');
    Route::resource('reportes', 'ReportesController');
    Route::resource('users', 'UserController');
    Route::resource('perfils', 'PerfilController');
    Route::get('/estrategias/finalizo/{id}', 'EstrategiasController@finalizo')->name('finalizo');
    Route::post('descarga', 'ReportesController@export')->name('descargaexcel');
});
